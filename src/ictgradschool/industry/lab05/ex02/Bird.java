package ictgradschool.industry.lab05.ex02;

/**
 * Represents a Bird.
 *
 * TODO Correctly implement these methods, as instructed in the lab handout.
 */
public class Bird implements IAnimal {

    String say;
    String name;

    @Override
    public String sayHello() {
        return say = "tweet tweet.";
    }

    @Override
    public boolean isMammal() {
        return false;
    }

    @Override
    public String myName() {
        return name = "Tweety the Bird";
    }

    @Override
    public int legCount() {
        return 2;
    }
}
