package ictgradschool.industry.lab05.ex02;

/**
 * Represents a dog.
 *
 * TODO Make this class implement the IAnimal interface, then implement all its methods.
 */
public class Dog implements IAnimal {

    String say;
    String name;

    @Override
    public String sayHello() {
        return say = "woof woof.";
    }

    @Override
    public boolean isMammal() {
        return true;
    }

    @Override
    public String myName() {
        return name = "Bruno the dog";
    }

    @Override
    public int legCount() {
        return 4;
    }
}
