package ictgradschool.industry.lab05.Ex03;

/*
public class Test1 extends SuperClass {
    int x2 = 20;
    static int y2 = 20;
    Test1 () {
        x2 = y2++;
    }
    public int foo2 () {
        return x2 ;
    }
    public static int goo2 () {
        return y2 ;
    }
    public static void main ( String [] args ) {
        SuperClass s1 = new SuperClass();
        Test1 t1 = new Test1();
        System.out.println( "The Base object" );
        System.out.println( "S1.x = " + s1.x );
        System.out.println( "S1.y = " + s1.y );
        System.out.println( "S1.foo() = " + s1.foo());
        System.out.println( "S1.goo() = " + s1.goo());
        System.out.println( "\nThe Derived object" );
        System.out.println( "\nInherited fields" );
        System.out.println( "T1.x = " + t1.x );
        System.out.println( "T1.y = " + t1.y );
        System.out.println( "T1.foo() = " + t1.foo());
        System.out.println( "T1.goo() = " + t1.goo());
        System.out.println( "\nThe instance/class fields" );
        System.out.println( "T1.x2 = " + t1.x2 );
        System.out.println( "T1.y2 = " + t1.y2 );
        System.out.println( "T1.foo2() = " + t1.foo2());
        System.out.println( "T1.goo2() = " + t1.goo2());
    }
}
*/

/*
The Base object
S1.x = 10
S1.y = 12
S1.foo() = 10
S1.goo() = 12

The Derived object

Inherited fields
T1.x = 11
T1.y = 12
T1.foo() = 11
T1.goo() = 12

The instance/class fields
T1.x2 = 20
T1.y2 = 21
T1.foo2() = 20
T1.goo2() = 21
 */


public class Test1 extends SuperClass{
    static int x =15;
    static int y = 15;
    int x2 = 20;
    static int y2 = 20;
    Test1() {
        x2 = y2++;
    }
    public int foo2 () {
        return x2;
    }
    public static int goo2 () {
        return y2;
    }
    public static int goo (){
        return y2;
    }
    public static void main ( String [] args ) {
        SuperClass s2 = new Test1 ();
       // Test1 t2 = (Test1) new SuperClass();
        System.out.println( "\nThe static Binding" );
        System.out.println( "S2.x = " + s2.x );
        System.out.println( "S2.y = " + s2.y );
        System.out.println( "S2.foo() = " + s2.foo());
        System.out.println( "S2.goo2() = " + s2.goo());
/*
        System.out.println( "\nThe static Binding" );
        System.out.println( "T2.x = " + t2.x );
        System.out.println( "T2.y = " + t2.y );
        System.out.println( "T2.foo() = " + t2.foo());
        System.out.println( "T2.goo2() = " + t2.goo());
        */

    }
}

/*
The Static Binding
S2.x = 10
S2.y = 11
S2.foo() = 10
S2.goo2() = 11
 */

/*
3. To which class is the method s2.goo() called?
    To the SuperClass

4. What is the static type of variable s2 ?
    SuperClass

5. Are we able to make a call to method foo2() from variable s2 ?
    No we are not able to.

6. What is the result from the following line of code?
Test1 t2 = new SuperClass ();
    Invalid as Test1 is the child of its parent SuperClass.  A variable cannot be created with the object SuperClass because Superclass is in the top hierarchy. Can only accept objects from class as new in a similar hierarchy or below.

7. What is the result from the following line of code?
Will receive an error, due to incompatible types, and due to trying to assign a higher hierarchy class object to a lower hierarchy class object.
Result.
Exception in thread "main" java.lang.ClassCastException: ictgradschool.industry.lab05.Ex03.SuperClass cannot be cast to ictgradschool.industry.lab05.Ex03.Test1
	at ictgradschool.industry.lab05.Ex03.Test1.main(Test1.java:77)

 */
