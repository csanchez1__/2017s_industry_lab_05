/*
public interface Scanner {
public Document getDocument ();
public boolean jobsDone ();
public Error getError ();
}
public interface Printer {
public void printDocument ( Document d );
public int getEstimateMinutesRemaining ();
public Error getError ();
}

public class PrintOmatic implements Printer {
// TODO Implement the necessary methods
    private int minutes;
    public Error error;
    public boolean jobs;

    public PrintOmatic(int minutes){
        this.minutes = minutes;
        error = new Error();
        jobs = false;
    }

    @Override
    public void printDocument( Document d){
        System.out.println("Document: " + d);
    }
    
    @Override
    public int getEstimateMinutesRemaining(){
        return minutes % 2;
    }

    @Override
    public Error getError(){
        return this.error;
    }
}
public class OverpricedAllInOnePrinterfier implements Scanner , Printer {
// TODO Implement the necessary methods

    Document document;
    private int minutes;
    public Error error;

    public OverpricedAllInOnePrinterfier(int minutes){
        document = new Document();
        this.minutes = minutes;
        error = new Error();

    }

    @Override
    public Document getDocument(){
        return document;
    }


    @Override
    public boolean jobsDone(){
            if(jobs == true){
                return true;
            }else 
            return false;
    }

    @Override
    public void printDocument( Document d){
        System.out.println("Document: " + d);
    }
    
    @Override
    public int getEstimateMinutesRemaining(){
        return minutes % 2;
    }

    @Override
    public Error getError(){
        return this.error;
    }
}


1. Scanner eg2 = new PrintOmatic (); //invalid
2. Printer eg3 = new OverpricedAllInOnePrinterfier ();//valid
3. OverPricedAllInOnePrinterfier eg4 = new Printer ();//invalid
4. Printer eg1 = new Scanner ();//invalid

*/
